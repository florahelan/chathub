package chathub;

import java.security.Key;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import chathub.CommandsProcessor.State;

public class ChatClient2 {
	public static String alicecert=null;
	public static void main(String[] args) throws Throwable {
		KeystoreResults kra = Crypto.keystoremethod(Constants.alicekeystore, Constants.alicepassword,
				Constants.alicealias);
		byte[] alicepubkey = kra.encodedPublicKey;
		Key aliceprivatekey = kra.PrivateKey;
		X509Certificate alicecertificate = kra.certificate;
		byte[] alicecertificate1 = alicecertificate.getEncoded();
		String b64CertKey1 = Base64.getEncoder().encodeToString(alicecertificate1);
		 Constants.alicecert=b64CertKey1;
		 
		Person alice = new Person("Alice", Constants.host, Constants.port1, State.INTIAL_STATE, alicepubkey,
				aliceprivatekey, alicecertificate, b64CertKey1);
		Person bob = new Person("Bob", Constants.host, Constants.port2, State.INTIAL_STATE, null, null, null, null);

		// Object creation for calling conversation class
		Conversation conversationWithAlice = new Conversation(alice, bob);

		// calls the start function from conversation class
		conversationWithAlice.start();
		try {
			System.out.println("> Welcome to Chat <");
			System.out.println("> This is Alice <" + "\n");
			String command;
			String serverGivenState = getMyState(alice.name, alice.certificatebase64);
			State myState = State.KA_WRITE_STATE.name().equals(serverGivenState) ? State.KA_WRITE_STATE
					: State.KA_READ_STATE;
			alice.updateState(myState);

			System.out.println("> My state is  = " + serverGivenState);
			if (State.KA_WRITE_STATE.name().equals(serverGivenState)) {
				boolean sessionEstablised = false;
				do {
					try {
						System.out.println(" Initiaing a Session with Bob ");
						sessionEstablised = conversationWithAlice.establishSession(State.KA_WRITE_STATE);
					} catch (Exception anyException) {
					}
				} while (!sessionEstablised);
			}
			// Gets input from the user till they type bye
			do {
				Scanner scanner = new Scanner(System.in);
				command = scanner.nextLine();
				if ("bye".equals(command)) {
					System.out.println("Good bye :)");
					break;
				}
				conversationWithAlice.sendMessage(command);
			} while (true);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// Close all the connection by end function in conversation class
			conversationWithAlice.end();
		}
	}
	//To check who intiated connection and start key agreement protocol accordingly
	private static String getMyState(String name, String base64pubKey) throws UnknownHostException, IOException {
		Socket sSocket = new Socket("localhost", Constants.server_port);
		OutputStream ostream = sSocket.getOutputStream();
		InputStream istream = sSocket.getInputStream();
		InputStreamReader streamReader = new InputStreamReader(istream);
		BufferedReader reader = new BufferedReader(streamReader);
		PrintWriter pwriter = new PrintWriter(ostream, true);
		String status = null;
		pwriter.println(Constants.connect + name + Constants.seperator + base64pubKey);
		do {
			try {
				status = reader.readLine();
			} catch (IOException e) {
				System.out.println("----- RUN SERVER BEFORE RUNNING CLIENT ----- ");
				e.printStackTrace();
			}
		} while (status == null);
		return status;
	}
}