package chathub;

import java.security.Key;
import java.security.cert.X509Certificate;

import chathub.CommandsProcessor.State;

public class Person {
	String name;
	int port;
	String address;
	String command;
	byte[] publickey;
	State currentState;
	Key privatekey;
	byte[] sessionKey;
	String certificatebase64;
	X509Certificate certificate;
	Person(String name, String address, int port, State startingState, byte[] publickey, Key privatekey,X509Certificate certificate,String certificatebase64) {
		this.name = name;
		this.address = address;
		this.port = port;
		this.currentState = startingState;
		this.publickey = publickey;
		this.privatekey = privatekey;
		this.certificate = certificate;
		this.certificatebase64 = certificatebase64;
	}

	public void updateState(State state) {
		this.currentState = state;
	}

	public State getState() {
		return currentState;
	}
}