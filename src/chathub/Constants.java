package chathub;

public final class Constants {
	
	final static String host = "localhost";
	final static int port1 = 6000;
	final static int port2 = 7000;
	final static int server_port = 8000;
	final static String Algorithm = "AES";
	final static String Algoinstance = "AES/GCM/NoPadding"; //"AES/CBC/PKCS5Padding";
	final static byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	final static int GCM_TAG_LENGTH = 16; // in bytes	
	final static String  noncestr = "AAAAAAAAAAAAAAAA";
	final static String cipher1 = "ecdh-secp256r1+x509+aes128/gcm128";
	final static String cipher2 = "ecdh-secp224r1+x509+aes128/gcm128";
	final static String KA = ":ka";// start key agreement with a list supported cipher suites
	final static String KA_OK = ":kaok";// acknowledge key agreement and select cipher suite
	final static String KA_1 = ":ka1";// “phase 1” of key agreement
	final static String ERR = ":err";// send message for recoverable error
	final static String FAIL = ":fail";// send message for non-recoverable error
	final static String cert = ":cert:";
	final static String certok = ":certok";
	final static String seperator = ":seperator:";
	final static String connect = ":connect:";
	final static String certnotok = ":certnotok";
	static String bobcert = " ";
	static String alicecert = " ";

	// Keystore related constants
	// server related
	final static char[] serverpassword = { 'S', 'e', 'r', 'v', 'e', 'r' };
	final static String serverkeystore = "chatserver.ks";
	final static String keystoreinstance = "JKS";
	final static String serveralias = "mykey-server";
	// alice related
	final static char[] alicepassword = { 'A', 'l', 'i', 'c', 'e', '1', '2', '3' };// Alice123
	final static String alicekeystore = "chatalice.jks";
	final static String alicealias = "mykey-alice";
	// bob related
	final static char[] bobpassword = { 'B', 'o', 'b', '1', '2', '3' };// Bob123
	final static String bobkeystore = "chatbob.jks";
	final static String bobalias = "mykey-bob";

}
