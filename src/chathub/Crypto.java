package chathub;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.*;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {

	static IvParameterSpec ivspec = new IvParameterSpec(Constants.iv);

	// Public Private Key Pair generator for Elliptic curve Instance
	static KeyPair keypairGen() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {

		KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC");
		ECGenParameterSpec ecsp = new ECGenParameterSpec("secp256r1");
		kpg.initialize(ecsp);
		KeyPair kp = kpg.genKeyPair();
		return kp;
	}

	// Encryption
	static String encrypt(String plaintext, byte[] sessionKey)  {
	    
        
        byte[] nonce = Base64.getDecoder().decode(Constants.noncestr);
       
        GCMParameterSpec spec = new GCMParameterSpec(Constants.GCM_TAG_LENGTH * 8, nonce);
		
		byte[] textInbytes = null;
		try {
			textInbytes = plaintext.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			System.out.println("Encoding type is not supported");
			e.printStackTrace();
			
		}
		Cipher cipherE = null;
		try {
			cipherE = Cipher.getInstance(Constants.Algoinstance);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			System.out.println("Algorithm or Padding Exception raised");
			e.printStackTrace();
		}
		SecretKey originalKey = null;
		try {
			originalKey = convertToKey(sessionKey);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			System.out.println("specification Exception");
			e.printStackTrace();
		}
		try {
			cipherE.init(Cipher.ENCRYPT_MODE, originalKey, spec);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			System.out.println("invalid parameter  or key");
			e.printStackTrace();
		}
		byte[] cipherText = null;
		try {
			cipherText = cipherE.doFinal(textInbytes);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			System.out.println("Bad Tag exception");
			e.printStackTrace();
		}
		String encryptedTextInBase64 = Base64.getEncoder().encodeToString(cipherText);
		return encryptedTextInBase64;
	}

	private static SecretKey convertToKey(byte[] sessionKey)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException {
		return new SecretKeySpec(sessionKey, Constants.Algorithm);
	}

	// Decryption
	static String decrypt(String base64, byte[] sessionKey)  {
        byte[] nonce = Base64.getDecoder().decode(Constants.noncestr);
        GCMParameterSpec spec = new GCMParameterSpec(Constants.GCM_TAG_LENGTH * 8, nonce);
		byte[] encryptedBytes = Base64.getDecoder().decode(base64);
		Cipher cipherD = null;
		try {
			cipherD = Cipher.getInstance(Constants.Algoinstance);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			System.out.println("Algorithm or Padding Exception raised");
			e.printStackTrace();
		}
		SecretKey originalKey = null;
		try {
			originalKey = convertToKey(sessionKey);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeySpecException e) {
			System.out.println("specification Exception");
			e.printStackTrace();
		}
		try {
			cipherD.init(Cipher.DECRYPT_MODE, originalKey, spec);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			System.out.println("invalid parameter  or key");
			e.printStackTrace();
		}
		byte[] decryptedBytes = null;
		try {
			decryptedBytes = cipherD.doFinal(encryptedBytes);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			System.out.println("Bad Tag exception");
			e.printStackTrace();
		}
		String plaintext = null;
		try {
			plaintext = new String(decryptedBytes, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			System.out.println("Encoding  Exception caught");
			e.printStackTrace();
		}
		return plaintext;
	}

	// ECDH symmetric session key generation
	static byte[] EcdhSharedSecretresult(byte[] ourPk, Key kp, String otherPkBase64)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {

		byte[] otherPkBytes = Base64.getDecoder().decode(otherPkBase64);
		KeyFactory kf = KeyFactory.getInstance("EC");
		X509EncodedKeySpec pkSpec = new X509EncodedKeySpec(otherPkBytes);
		PublicKey otherPublicKey = null;
		try {
			otherPublicKey = kf.generatePublic(pkSpec);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}

		// Perform key agreement
		KeyAgreement ka = KeyAgreement.getInstance("ECDH");
		try {
			ka.init(kp);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}
		try {
			ka.doPhase(otherPublicKey, true);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
		// Read shared secret
		byte[] sharedSecret = ka.generateSecret();
		// Base64 Encoded
		String sharedSecret2 = Base64.getEncoder().encodeToString(sharedSecret);
		//System.out.println(sharedSecret2);

		// Derive a key from the shared secret and both public keys
		MessageDigest hash = MessageDigest.getInstance("SHA-256");
		hash.update(sharedSecret);

		// Simple deterministic ordering
		List<ByteBuffer> keys = Arrays.asList(ByteBuffer.wrap(ourPk), ByteBuffer.wrap(otherPkBytes));
		Collections.sort(keys);
		hash.update(keys.get(0));
		hash.update(keys.get(1));

		byte[] derivedKey = hash.digest();

		byte[] derived_sixteen = new byte[16];
		System.arraycopy(derivedKey, 0, derived_sixteen, 0, 16);
		System.out.println("sessionkey:"+Base64.getEncoder().encodeToString(derived_sixteen));
		return derived_sixteen;
	}

	// keystore file key pair and certificate generation
	static KeystoreResults keystoremethod(String keystore, char[] password, String alias) throws Exception {

		// Taking key store instance as "JKS"
		KeyStore ks = KeyStore.getInstance(Constants.keystoreinstance);

		// get user password and file input stream

		java.io.FileInputStream fis = null;
		try {
			fis = new java.io.FileInputStream(keystore);
			ks.load(fis, password);
		} finally {
			if (fis != null) {
				fis.close();
			}
		}

		// Changing password
		KeyStore.PasswordProtection passwordProtection = null;
		passwordProtection = new KeyStore.PasswordProtection(password);
		// get my private key
		KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(alias, passwordProtection);
		PrivateKey myPrivateKey = pkEntry.getPrivateKey();
		X509Certificate cert = (X509Certificate) ks.getCertificate(alias);
		PublicKey pubkeyentry = cert.getPublicKey();
		byte[] encodedPublicKey = pubkeyentry.getEncoded();
		return new KeystoreResults(encodedPublicKey, myPrivateKey, cert);
	}

	// certificate and signature verify function
	static String certsignverifier(String name, X509Certificate certificate, boolean CAverfy, String publickey) {
		// checking CA verified certificate or not
		if (CAverfy) {
			// checking validity of certificate
			Date today = new Date();
			if (certificate instanceof X509Certificate) {
				try {
					((X509Certificate) certificate).checkValidity();
					// CN check
					String[] cernamesplit = certificate.getSubjectX500Principal().getName().split(",");
					String commonname = cernamesplit[0].substring(3);
					// checking common name from certificate matches to the requested person or not
					if (commonname.equalsIgnoreCase(name)) {
						// check for keyexchange data
						return null;
					} else {
						return "CN unmatched";
					}
				} catch (CertificateExpiredException | CertificateNotYetValidException cee) {
					System.out.println("Certificate is expired");
					return "Certificate is expired or not valid";
				}
			}
			// certificate.verify(key);
		} else {
			System.out.println("Certificate is not verified by chat server");
			return "Certificate is not verified by chat server";
		}
		return null;
	}
	}