package chathub;

import java.security.Key;
import java.security.cert.X509Certificate;
//used to return key pair object  from keystore file
public class KeystoreResults {
	byte[] encodedPublicKey;
	Key PrivateKey;
	X509Certificate certificate;
	public KeystoreResults(byte[] encodedPublicKey,Key PrivateKey,X509Certificate certificate) {
		this.encodedPublicKey = encodedPublicKey;
		this.PrivateKey = PrivateKey;
		this.certificate = certificate;
	}
}
