package chathub;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import chathub.CommandsProcessor.State;

public class Server {

	private Socket sendSocket;
	private OutputStream ostream;
	private volatile InputStream istream;
	private PrintWriter pwriter;
	private volatile BufferedReader reader;
	private ServerSocket listenSocket;
	Person me, other_person;
	boolean processStarted = false;
	static int counter = 0;
	List<State> initialStates = Arrays.asList(State.KA_READ_STATE, State.KA_WRITE_STATE);
	Map<String, String> userNameCertMapping = new HashMap<>();

	public static void main(String args[]) throws Throwable {
		Server S = new Server();
		System.out.println("Started");
		S.beginToListen();
	}

	private void addCert(String name, String base64cert) {
		userNameCertMapping.put(name, base64cert);
	}

	public void beginToListen() throws Throwable {
		// Port established for server
		ServerSocket listenSocket = new ServerSocket(Constants.server_port);
		// Server is ready to listen and new thread is created.
		while (true) {
			System.out.println("Waiting for a connection...");
			final Socket socket = listenSocket.accept();
			System.out.println("Server Connected...");
			Runnable listenRunnable = new Runnable() {
				public void run() {
					try {
						readUnencrpyted(socket);
					} catch (Exception e) {
						System.out.println("Cannot acccept new connections");
						e.printStackTrace();
					}
				}
			};
			Thread workerThread = new Thread(listenRunnable);
			// Thread is started
			workerThread.start();
		}
	}
	//reads the certificate from clients and validates with the certificate that server is having already
	public void readUnencrpyted(Socket socket) {
		InputStream istream = null;
		try {
			istream = socket.getInputStream();
			OutputStream ostream = socket.getOutputStream();
			PrintWriter pwriter = new PrintWriter(ostream, true);
			BufferedReader reader = new BufferedReader(new InputStreamReader(istream));
			System.out.println("Reading ..  ");
			String messageFromClient = null;
			do {
				messageFromClient = reader.readLine();
			} while (messageFromClient == null);
			if (messageFromClient.startsWith(Constants.connect)) {
				String replacedCommandString = messageFromClient.replace(Constants.connect, "");
				// Reading cert from client
				List<String> userCertList = Arrays.asList(replacedCommandString.split(Constants.seperator));
				String userName = userCertList.get(0);
				String base64cert = userCertList.get(1);
				addCert(userName, base64cert);
				String state = initialStates.get((counter++ % 2)).name();
				pwriter.println(state);
			} else if (messageFromClient.startsWith(Constants.cert)){
				String replacedCommandString = messageFromClient.replace(Constants.cert, "");
				List<String> certInfoList = Arrays.asList(replacedCommandString.split(Constants.seperator));
				String userName = certInfoList.get(0);
				String base64cert = certInfoList.get(1);
				if (validateCert(userName, base64cert) == true) {
					System.out.println("Certificate  of  " + userName+"is verified succefully");
					pwriter.println(Constants.certok);
				} else {
					System.out.println("certificate is not of  " + userName);
					pwriter.println(Constants.certnotok);
				}
			} else {
				System.out.println("Unknown command to server " + messageFromClient);
				pwriter.println("You are a hacker !!");
			}
			socket.close();
		} catch (IOException ioExc) {
			ioExc.printStackTrace();
		} finally {
			try {
				if (istream != null) {
					istream.close();
				}
				if (ostream != null) {
					ostream.close();
				}
			} catch (IOException e) {
				System.out.println("Cannot close the connnection");
			}
		}
	}

	private boolean validateCert(String userName, String base64cert) {
		String storedCertForUser = userNameCertMapping.get(userName);
		if (storedCertForUser == null) {
			return false;
		}
		return base64cert.equals(storedCertForUser);
	}

	// closes all the connection
	public void end() throws IOException {

		if (sendSocket != null) {
			if (ostream != null) {
				ostream.close();
			}
			if (pwriter != null) {
				pwriter.close();
			}
			sendSocket.close();
		}

		if (listenSocket != null) {
			listenSocket.close();
			if (istream != null) {
				istream.close();
			}
		}

	}
}