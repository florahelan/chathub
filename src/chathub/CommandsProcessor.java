package chathub;

public class CommandsProcessor {
	enum State {
		KA_WRITE_STATE(":ka"), KA_READ_STATE(":ka"),

		READ_ACK_STATE(":kaok"), WRITE_ACK_STATE(":kaok"),
		
		KA_1_READ_STATE(":ka1"), KA_1_WRITE_STATE(":ka1"),

		DATA_TRANSFER("data"), INTIAL_STATE("unknown"),

		ERROR("err");

		private String stateStr;

		State(String stateStr) {
			this.stateStr = stateStr;
		}
	}

	public static State getNextState(State currentState) {
		switch (currentState) {
		// Read States
		// Alice
		case KA_READ_STATE:
			return State.KA_1_READ_STATE;
		case KA_1_READ_STATE:
			// Session key generation
			return State.DATA_TRANSFER;

		// Bob
		case KA_WRITE_STATE:
			return State.KA_1_WRITE_STATE;
		case KA_1_WRITE_STATE:
			return State.DATA_TRANSFER;

		default:
			return State.ERROR;
		}
	}
}