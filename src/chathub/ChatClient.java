package chathub;

import java.io.InputStream;
import java.security.Key;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import chathub.CommandsProcessor.State;

public class ChatClient {
	public static void main(String[] args) throws Throwable {
		KeystoreResults krb = Crypto.keystoremethod(Constants.bobkeystore, Constants.bobpassword, Constants.bobalias);
		byte[] bobpubkey = krb.encodedPublicKey;
		Key bobprivatekey = krb.PrivateKey;
		X509Certificate bobcertificate = krb.certificate;
		byte[] bobcertificate1 = bobcertificate.getEncoded();
		//certificate is generated for bob
		String b64CertKey = Base64.getEncoder().encodeToString(bobcertificate1);
		Constants.bobcert = b64CertKey;
		Person alice = new Person("Alice", Constants.host, Constants.port1, State.INTIAL_STATE, null, null, null, null);
		Person bob = new Person("Bob", Constants.host, Constants.port2, State.INTIAL_STATE, bobpubkey, bobprivatekey,
				bobcertificate, b64CertKey);

		System.out.println("data:" + bobpubkey + "    " + bobprivatekey);

		// Object creation for calling conversation class
		Conversation conversationWithBob = new Conversation(bob, alice);

		// calls the start function from conversation class
		conversationWithBob.start();
		String serverGivenState = getMyState(bob.name, bob.certificatebase64);
		State myState = State.KA_WRITE_STATE.name().equals(serverGivenState) ? State.KA_WRITE_STATE
				: State.KA_READ_STATE;
		bob.updateState(myState);

		System.out.println("> My state is  = " + serverGivenState);

		try {
			System.out.println("> Welcome to Chat <");
			System.out.println("> This is Bob <" + "\n");
			String command;

			if (myState.equals(State.KA_WRITE_STATE)) {
				boolean sessionEstablised = false;
				do {
					try {
						System.out.println(" Initiaing a Session with Alice ");
						sessionEstablised = conversationWithBob.establishSession(State.KA_WRITE_STATE);
					} catch (Exception anyException) {
					}
				} while (!sessionEstablised);
			}
			
			// Gets input from the user till they type bye
			do {
				Scanner scanner = new Scanner(System.in);
				command = scanner.nextLine();
				if ("bye".equals(command)) {
					System.out.println("Good bye :)");
					break;
				}
				conversationWithBob.sendMessage(command);
			} while (true);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// Close all the connection by end function in conversation class
			conversationWithBob.end();
		}
	}
//To check who intiated connection and start key agreement protocol accordingly
	private static String getMyState(String name, String base64pubKey) throws UnknownHostException, IOException {
		Socket sSocket = new Socket("localhost", Constants.server_port);
		OutputStream ostream = sSocket.getOutputStream();
		InputStream istream = sSocket.getInputStream();
		InputStreamReader streamReader = new InputStreamReader(istream);
		BufferedReader reader = new BufferedReader(streamReader);
		PrintWriter pwriter = new PrintWriter(ostream, true);
		String status = null;
		pwriter.println(Constants.connect + name + Constants.seperator + base64pubKey);
		do {
			try {
				status = reader.readLine();
			} catch (IOException e) {
				System.out.println("----- RUN SERVER BEFORE RUNNING CLIENT ----- ");
				e.printStackTrace();
			}
		} while (status == null);
		return status;
	}
}