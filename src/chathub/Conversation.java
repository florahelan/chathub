package chathub;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

import chathub.CommandsProcessor.State;

public class Conversation {

	private Socket sendSocket;
	private OutputStream ostream;
	private volatile InputStream istream;
	private PrintWriter pwriter;
	private volatile BufferedReader reader;
	private ServerSocket listenSocket;
	Person me, other_person;

	Conversation(Person me, Person other) {
		this.me = me;
		this.other_person = other;
		this.me.publickey = me.publickey;
		this.me.privatekey = me.privatekey;
		this.me.certificate = me.certificate;
		this.me.certificatebase64 = me.certificatebase64;
	}

	// Thread is used to accept new correction
	public void start() throws Exception {
		Runnable listenRunnable = new Runnable() {
			public void run() {
				try {
					// Calls beginToListen method
					beginToListen();
					System.out.println("Connected to server");
				} catch (Throwable e) {
					if (e instanceof SocketException) {
						System.out.println("> Leaving Chat Client <" + e.getMessage());
					} else {
						e.printStackTrace();
					}
				} finally {
					System.out.println("");
				}
			}
		};
		Thread workerThread = new Thread(listenRunnable);
		workerThread.start();
	}

	// Establishes the port connection
	private void beginToListen() throws Throwable {
		listenSocket = new ServerSocket(me.port);
		System.out.println(me.name + " is waiting to connect with " + other_person.name);
		// waits for the connection to arrive on this port.
		Socket socket = listenSocket.accept();
		// gets the input from user
		istream = socket.getInputStream();
		// Reads the message from the other user
		State state = me.getState();
		// changed
		if (state != State.DATA_TRANSFER) {
			if (state == State.KA_READ_STATE || state == State.READ_ACK_STATE) {
				if (establishSession(me.getState())) {
					readEncryptedMessage();
				}

			}
		}
	}

	public boolean establishSession(State currentState) throws Throwable {
		// Alice State Changes
		// Read :ka
		if (currentState == State.KA_READ_STATE) {
			String message = readUnencrpyted();
			// Check if message is of the format :ka1
			if (!((message.substring(0, 3)).equals(Constants.KA))) {
				System.out.println(Constants.FAIL + "  " + "Command did not start with :ka");
				return false;
			}
			// send kaOk by confirming the required cipher suite is there or not
			String[] splitedka1 = message.split("\\s+");
			
			for(int i=0;i<3;i++)
			{
				String ciphesuite = splitedka1[i]; 
				if(ciphesuite.equals(Constants.cipher2))
				{			
				String sendMessage = Constants.KA_OK + "  " + Constants.cipher2;
				sendUnEncryptedMessage(sendMessage);
				}
				else if(i==2)
				{
					System.out.println(Constants.FAIL + "  " + "Required cipher suite is not in the list to select for Kaok");
					return false;
				}
				
				
			}
				
			// send ka1
			// Base64 Encoded
			String pubkey1 = Base64.getEncoder().encodeToString(me.publickey);
			String ka1 = Constants.KA_1 + " " + pubkey1 + " " + ":cert" + " " + me.certificatebase64;
			// Base64 Decoded

			sendUnEncryptedMessage(ka1);
			me.updateState(CommandsProcessor.getNextState(currentState));
			return establishSession(me.getState());
		}

		// Reads :ka1 with bob's public key
		if (currentState == State.KA_1_READ_STATE) {
			String keyAgreementString = readUnencrpyted();
			// Check if string is of the format ka1:public Key
			if ((keyAgreementString.substring(0, 4)).equals(Constants.KA_1) != true) {
				System.out.println(Constants.FAIL + "  " + "Command did not start with :ka1");
				return false;
			}
			// Base64 Decoded
			String[] splitedka1 = keyAgreementString.split("\\s+");
			String certificate1 = splitedka1[3]; 
			// Connect to server
			System.out.println("Connecting to server");
			Socket sSocket = new Socket("localhost", Constants.server_port);
			OutputStream ostream = sSocket.getOutputStream();
			InputStream istream = sSocket.getInputStream();
			InputStreamReader streamReader = new InputStreamReader(istream);
			BufferedReader reader = new BufferedReader(streamReader);

			PrintWriter pwriter = new PrintWriter(ostream, true);
			String certmessage = Constants.cert + other_person.name + Constants.seperator + certificate1;
			String certstatus = null;
			pwriter.println(certmessage);
			do {
				try {
					certstatus = reader.readLine();
				} catch (IOException e) {
					System.out.println("Errror " + e.getMessage());
					e.printStackTrace();
				}
			} while (certstatus == null);

			System.out.println(" Certificate status " + certstatus); // This receives approval that certificate is valid
			if (certstatus.equals(Constants.certok)) {
				boolean cacrtificatevalid = true; // As we are entering here if CA verifies only
				String pubkeysub = splitedka1[1]; 
				// Need to verify with the server that the received public key along with the certificate is authorized.
				byte[] certbytes1 = Base64.getDecoder().decode(certificate1);
				
				CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
				InputStream in = new ByteArrayInputStream(certbytes1);
				X509Certificate othercert2 = (X509Certificate) certFactory.generateCertificate(in);
				String verifymsg = Crypto.certsignverifier(other_person.name, othercert2, cacrtificatevalid, pubkeysub);

				
				if (verifymsg == null) {
					me.sessionKey = Crypto.EcdhSharedSecretresult(me.publickey, me.privatekey, pubkeysub);
					// Base64 Encoded
					me.updateState(CommandsProcessor.getNextState(currentState));
					return establishSession(me.getState());
				} else {
					System.out.println(" verification of certificate and signature failed ");
					return false;
				}
			} else {
				System.out.println(" Certificate verify failed ");
				end();
			}
		}

		// Bob State Changes
		if (currentState == State.KA_WRITE_STATE) {
			String message = Constants.KA + " " + Constants.cipher1 + " " + Constants.cipher2;
			// Send my session key
			sendUnEncryptedMessage(message);
			// Read ka ok
			String kaOkString = readUnencrpyted();
			// Is kaOk string matches

			if ((kaOkString.substring(0, 5)).equals(Constants.KA_OK) != true) {
				System.out.println(Constants.FAIL + "  " + "Command did not start with :kaok");
				return false;
			}
			// Read ka1 with alice's public key
			String ka1String = readUnencrpyted();
			// Is ka1With Public Key present
			if ((ka1String.substring(0, 4)).equals(Constants.KA_1) != true) {
				System.out.println(Constants.FAIL + "  " + "Command did not start with :ka1");
				return false;
			}
			String[] splitedka1 = ka1String.split("\\s+");
			String certificate2 = splitedka1[3];
			// Connect to server
			System.out.println("Connecting to server");
			Socket sSocket = new Socket("localhost", Constants.server_port);
			OutputStream ostream = sSocket.getOutputStream();
			InputStream istream = sSocket.getInputStream();
			InputStreamReader streamReader = new InputStreamReader(istream);
			BufferedReader reader = new BufferedReader(streamReader);

			PrintWriter pwriter = new PrintWriter(ostream, true);
			String certmessage = Constants.cert + other_person.name + Constants.seperator + certificate2;
			String certstatus = null;
			pwriter.println(certmessage);
			do {
				try {
					certstatus = reader.readLine();
				} catch (IOException e) {
					System.out.println("Errror " + e.getMessage());
					e.printStackTrace();
				}
			} while (certstatus == null);

			System.out.println(" Certificate status " + certstatus); // this receives approval that certificate is valid
			if (certstatus.equals(Constants.certok)) {
				String pubkeysub2 = splitedka1[1]; 
				// Need to verify with the server that the received public key along with the
				// certificate is authorized.
				// method to verify certificate and signature
				byte[] certbytes = Base64.getDecoder().decode(certificate2);
				CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
				InputStream in = new ByteArrayInputStream(certbytes);
				X509Certificate othercert = (X509Certificate) certFactory.generateCertificate(in);
				String verifymsg = Crypto.certsignverifier(other_person.name, othercert, true, pubkeysub2);
				if (verifymsg == null) {
					me.sessionKey = Crypto.EcdhSharedSecretresult(me.publickey, me.privatekey, pubkeysub2);
					// Base64 Encoded
					me.updateState(CommandsProcessor.getNextState(currentState));
					return establishSession(me.getState());
				} else {
					System.out.println(" verification of certificate and signature failed ");
					return false;
				}
			} else {
				System.out.println(" Certificate verify failed");
				end();
			}
		}

		if (currentState == State.KA_1_WRITE_STATE) {
			// sending bob's public key
			// Base64 Encoded
			String pubkey2 = Base64.getEncoder().encodeToString(me.publickey);
			String message = Constants.KA_1 + " " + pubkey2 + " " + ":cert" + " " + me.certificatebase64; // Bob's
																											// public
																											// key
			sendUnEncryptedMessage(message);
			//String otherSessionKey = "SESSION FROM " + other_person.name;// generate from elliptical function
			me.updateState(CommandsProcessor.getNextState(currentState));
			return establishSession(me.getState());
		}

		if (currentState == State.ERROR) {
			return false;
		}
		if (currentState == State.DATA_TRANSFER) {
			// Session is now established .
			System.out.println("session key has been established");
			System.out.println("All set to chat with " + other_person.name);
			startReadThread();
			return true;
		}
		return false;
	}

	private void startReadThread() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					readEncryptedMessage();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	// Prepares the stream to send message
	private void beginToSend() throws Throwable {
		sendSocket = new Socket(other_person.address, other_person.port);
		ostream = sendSocket.getOutputStream();
		pwriter = new PrintWriter(ostream, true);
	}

	// Sends the message on the output stream
	void sendMessage(String message) throws Throwable {
		if (pwriter == null) {
			beginToSend();
		}

		// calls encrypt function from crypto class to encrypt the message
		String ciphertext = Crypto.encrypt(message, me.sessionKey);
		// message is encrypted and stored in ciphertext
		System.out.println("Encrypted message: " + ciphertext + "\n");
		pwriter.println(ciphertext);
	}

	private void sendUnEncryptedMessage(String message) throws Throwable {
		if (pwriter == null) {
			beginToSend();
		}
		System.out.println("Sending >>>>>> " + message);
		System.out.println("================================================");
		pwriter.println(message);
	}

	// reads the unencrypted messages and decrypt it
	private String readUnencrpyted() {
		// message is stored in clientMessage variable
		if (reader == null) {
			while (istream == null) {
				;
			}
			InputStreamReader streamReader = new InputStreamReader(istream);
			reader = new BufferedReader(streamReader);
		}
		String unEncryptedMessage = null;
		do {
			try {
				unEncryptedMessage = reader.readLine();
			} catch (IOException e) {
				System.out.println("Errror " + e.getMessage());
				e.printStackTrace();
				break;
			}

		} while (unEncryptedMessage == null);
		System.out.println("Reading <<<< " + unEncryptedMessage);
		System.out.println("================================================");
		return unEncryptedMessage;
	}

	// reads the encrypted messages and decrypt it
	private void readEncryptedMessage() throws Throwable {
	// message is stored in clientMessage variable
		BufferedReader clientMessage;
		if (reader == null) {
			clientMessage = new BufferedReader(new InputStreamReader(istream));
		} else {
			clientMessage = reader;
		}
		String encryptedMessage, plaintext = "";
		do {
			if ((encryptedMessage = clientMessage.readLine()) != null) {
				System.out.println("Received Message: " + encryptedMessage + "\n");
				// Calls decrypt function from crypto class to decrypt the message
				plaintext = Crypto.decrypt(encryptedMessage.trim(), me.sessionKey);
				// message is decrypted and stored in plaintext
				System.out.println("From " + other_person.name + ": " + plaintext + "\n");
			}
		} while (!"bye".equals(plaintext));
		System.out.println("Good bye :)");
		end();
	}

	// closes all the connection
	public void end() throws IOException {
		if (sendSocket != null) {
			if (ostream != null) {
				ostream.close();
			}
			if (pwriter != null) {
				pwriter.close();
			}
			sendSocket.close();
		}

		if (listenSocket != null) {
			listenSocket.close();
			if (istream != null) {
				istream.close();
			}
		}

	}
}